# Fake Minesweeper
## Team member names:
Zac Andersen, Logan Atkinson, William Hay

## Estimation on completion time:
 - Zac: 3 hours
- Logan: 5 hours
- William: 3 hours

## Shortcomings/concerns:
 - Zac: No concerns with the project as a whole.
 - Logan: On the personal side, really only taking I/O from CMD,  which is still not fully functioning. On the group side unit testing for I/O.
 - William: Some concerns I had was mainly with my countBombs method, and the process of how I was going to check all adjacent spaces, which I was able to accomplish.