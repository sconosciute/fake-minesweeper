package andersenz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Zac's minesweeper hint generator solution
 * Generates fields of hints from given .* syntax command line input
 * @version Winter 2024
 * @author Z Andersen
 */
public class andersenz {
    static List<String> inputLines = new ArrayList<>();
    private static final ArrayList<char[][]> INFIELDS = new ArrayList<>();
    private static final ArrayList<char[][]> OUTFIELDS = new ArrayList<>();

    public static void main(String[] args){
        readInput();
        formatInput();
        for (char[][] in: INFIELDS) {
            if (in.length == 0) continue;
            buildOutput(in);
        }
        int count = 1;
        for (char[][] out : OUTFIELDS) {
            writeOutput(out, count);
            count++;
        }
    }

    public static void readInput(){
        Scanner s = new Scanner(System.in);
        String endCheck = "0 0";
        System.out.println("ready");
        while (s.hasNextLine()) {
            String in = s.nextLine();
            if (in == null || in.isEmpty() || in.equals(endCheck)) break;
            inputLines.add(in);
        }
    }

    public static void writeOutput(char[][] theOutfield, int count) {


        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Field #%d:%n", count));
        for (char[] chars : theOutfield) {
            for (char c : chars) {
                sb.append(c);
            }
            sb.append("\n");
        }
        System.out.println(sb);
    }

    public static void formatInput() {
        int rows = 0;
        int cols;
        for (int i = 0; i < inputLines.size(); i++) {
            String ln = inputLines.get(i);
            if (isNum(ln)) {
                StringBuilder num = new StringBuilder();
                for (char c : ln.toCharArray()) {
                    if (c == ' ') {
                        rows = Integer.parseInt(num.toString());
                        num = new StringBuilder();
                        continue;
                    }
                    num.append(c);
                }
                cols = Integer.parseInt(num.toString());
                INFIELDS.add(formatter(rows, cols, i + 1));
            }
        }
    }

    private static char[][] formatter(int theRows, int theCols, int theIndex) {
        char[][] in = new char[theRows][theCols];

        String line;
        for (int row = theIndex; row < theIndex + theRows; row++) {
            line = inputLines.get(row);
            for (int col = 0; col < theCols; col++) {
                in[row - theIndex][col] = line.charAt(col);
            }
        }

        return in;
    }

    private static boolean isNum(String theStr) {
        return Character.isDigit(theStr.charAt(0));
    }

    public static void buildOutput(char[][] theInfield) {
        char[][] out = new char[theInfield.length][theInfield[0].length];
        for (char[] a : out) {
            Arrays.fill(a, '0');
        }

        for (int i = 0; i < theInfield.length; i++) {
            for (int j = 0; j < theInfield[0].length; j++){
                char c = theInfield[i][j];
                if (c == '*') {
                    out[i][j] = '*';
                    countBombs(i, j, out);
                }

            }

        }

        OUTFIELDS.add(out);
    }

    private static void countBombs(int theRow, int theCol, char[][] theInfield) {
        for (int row = theRow-1; row <= theRow+1; row++) {
            if (row < 0 || row >= theInfield.length) continue;
            for (int col = theCol-1; col <= theCol+1; col++) {
                if (col < 0 || col >= theInfield[0].length || (row == theRow && col == theCol)
                        || theInfield[row][col] == '*') continue;
                theInfield[row][col] += 1;
            }
        }
    }

}