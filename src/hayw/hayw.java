// Made by William Hay

package hayw;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class hayw {
    public static void main(String[] theArgs) throws FileNotFoundException {
        Scanner input = new Scanner(System.in);
        int count = 1;

        if (theArgs.length == 1) {
            input = new Scanner(new File(theArgs[0]));
        }

        while (true) {
            int n = input.nextInt();
            int m = input.nextInt();

            if (n == 0 && m == 0) {
                break;
            }

            char[][] field = readField(input, n, m);

            System.out.println(processField(field, count));

            count++;
        }
        input.close();
    }

    private static char[][] readField(Scanner theScanner, int theRow, int theCol) {
        char[][] field = new char[theRow][theCol];

        for (int i = 0; i < theRow; i++) {
            String row = theScanner.next();
            field[i] = row.toCharArray();
        }
        return field;
    }

    private static String processField(char[][] theField, int theCount) {
        StringBuilder result = new StringBuilder("Field #" + theCount + ":\n");

        for (int i = 0; i < theField.length; i++) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j < theField[i].length; j++) {
                if (theField[i][j] == '.') {
                    int minesCount = countMines(theField, i, j);
                    row.append(minesCount);
                } else {
                    row.append('*');
                }
            }
            result.append(row).append("\n");
        }
        return result.toString();
    }

    private static int countMines(char[][] theField, int theRow, int theCol) {
        int bombCount = 0;
        int[][] directions = {
                {-1, -1}, {-1, 0}, {-1, 1},
                {0, -1},           {0, 1},
                {1, -1}, {1, 0}, {1, 1}
        };

        for (int[] direction : directions) {
            if (theRow + direction[0] >= 0 && theRow + direction[0] < theField.length &&
                    theCol + direction[1] >= 0 && theCol + direction[1] < theField[theRow].length &&
                    theField[theRow + direction[0]][theCol + direction[1]] == '*') {
                bombCount++;
            }
        }
        return bombCount;
    }
}