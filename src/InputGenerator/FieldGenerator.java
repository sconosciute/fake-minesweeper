package InputGenerator;

import java.util.Random;

public class FieldGenerator {
    /**
     * Generates test fields for fake minesweepers
     * Args: 0 = 1-100 width, 1 = 1-100 height, 2 = 0-100% Chance for bomb
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            throw new IllegalArgumentException("Insufficient Arguments.");
        }
        int rows = Integer.parseInt(args[0]);
        int cols = Integer.parseInt(args[1]);
        int bombChance = Integer.parseInt(args[2]);

        if (rows > 100 || cols > 100) {
            throw new IllegalArgumentException(String.format("Size out of bounds, min 1x1, max 100x100.\nYou asked " +
                    "for %d x %d", rows, cols));
        }

        if (bombChance < 0 || bombChance > 100) {
            throw new IllegalArgumentException("Bomb chance must be between 0 and 100");
        }

        char[][] field = new char[rows][cols];
        Random rand = new Random();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int bombProc = rand.nextInt(101);
                if (bombProc <= bombChance && bombChance != 0) {
                    field[i][j] = '*';
                } else {
                    field[i][j] = '.';
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (char[] chars : field) {
            for (char c : chars) {
                sb.append(c);
            }
            sb.append("\n");
        }
        System.out.println(sb);

    }
}
