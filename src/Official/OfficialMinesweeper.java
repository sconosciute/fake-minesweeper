package Official;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Official Minesweeper hint generator solution.
 * Takes command line input to generate hints for a minesweeper field.
 * @version Winter 2024
 * @author Z Andersen
 * @author W Hay
 * @author L Atkinson
 */
public class OfficialMinesweeper {
    public static final ArrayList<char[][]> INFIELDS = new ArrayList<>();
    public static final ArrayList<char[][]> OUTFIELDS = new ArrayList<>();
    public static void main(String[] theArgs) {
        Scanner s = new Scanner(System.in);
        readFormatInput(s);
        for (char[][] in : INFIELDS) {
            OUTFIELDS.add(computeOutput(in));
        }
        writeOutput();
    }

    /**
     * Reads input from command line and formats for computer
     */
    public static void readFormatInput(final Scanner theInput) {

        int rows = theInput.nextInt();
        int cols = theInput.nextInt();
        while (rows != 0 && cols != 0) {
            INFIELDS.add(formatter(rows, cols, theInput));
            rows = theInput.nextInt();
            cols = theInput.nextInt();
        }

    }

    /**
     * Formats individual field for input.
     * @param theRows Numb rows in field
     * @param theCols Num columns in field
     * @param theInput Scanner containing input data.
     * @return Formatted 2D char array containing input data.
     */
    private static char[][] formatter(final int theRows, final int theCols, final Scanner theInput) {
        char[][] field = new char[theRows][theCols];
        for (int i = 0; i < theRows; i++) {
            field[i] = theInput.next().toCharArray();
        }
        return field;
    }

    /**
     * Outputs computed hint fields
     */
    public static void writeOutput() {
        int count = 1;
        StringBuilder sb = new StringBuilder();
        for (char[][] out : OUTFIELDS) {
            for (char[] c : out) {
                sb.append(new String(c));
                sb.append("\n");
            }
            System.out.printf("Field #%d:\n", count);
            System.out.println(sb);
            sb.setLength(0);
            sb.trimToSize();
            count++;
        }

    }

    /**
     * Computes hint field output from .* syntax input
     * @param theInfield Minefield to compute hints for
     * @return Computed hint field for output
     */
    public static char[][] computeOutput(final char[][] theInfield) {
        char[][] out = new char[theInfield.length][theInfield[0].length];
        for (char[] a : out) {
            Arrays.fill(a, '0');
        }

        for (int i = 0; i < theInfield.length; i++) {
            for (int j = 0; j < theInfield[0].length; j++) {
                char c = theInfield[i][j];
                if (c == '*') {
                    out[i][j] = '*';
                    countBombs(i, j, out);
                }

            }

        }
        return out;
    }

    /**
     * Increments adjacent hint tiles by 1 from given bomb coordinate in given field
     * @param theRow Bomb row position
     * @param theCol Bomb column position
     * @param theInfield Field to compute in.
     */
    private static void countBombs(final int theRow, final int theCol, final char[][] theInfield) {
        for (int row = theRow-1; row <= theRow+1; row++) {
            if (row < 0 || row >= theInfield.length) continue;
            for (int col = theCol-1; col <= theCol+1; col++) {
                if (col < 0 || col >= theInfield[0].length || theInfield[row][col] == '*') continue;
                theInfield[row][col] += 1;
            }

        }

    }

}
