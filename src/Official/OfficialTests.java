package Official;
//import org.junit.jupiter.*;
//import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Scanner;

import static Official.OfficialMinesweeper.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OfficialTests {
    final InputStream sysInBackup = System.in;
    final OutputStream sysOutBackup = System.out;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    //@Rule
    //    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @BeforeEach
    public void setUp() {
        System.setIn(sysInBackup);
        System.setOut((PrintStream) sysOutBackup);
    }
    @Test
    public void readFormatInputTest2x2() {

        //Found simulating user input https://stackoverflow.com/questions/6415728/junit-testing-with-simulated-user-input

        /*
        InputStream sysInBackup = System.in; // backup System.in to restore it later
        ByteArrayInputStream in = new ByteArrayInputStream("My string".getBytes());
        System.setIn(in);
         */
        InputStream sysInBackup = System.in; // backup System.in to restore it later
        ByteArrayInputStream in = new ByteArrayInputStream("2 2\n..\n..\n0 0".getBytes());
        System.setIn(in);
        //Scanner sc = System.setIn(in);
        Scanner sc = new Scanner(System.in);
        String answer = "..\n..\n";
        readFormatInput(sc);
        String testString = "";
        if (!INFIELDS.isEmpty()) {
            char[][] test = OfficialMinesweeper.INFIELDS.get(0);
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    testString += test[i][j];
                }
                testString += "\n";
            }
        }
        //INFIELDS.clear();
        assertEquals(answer, testString);
    }

    @Test
    public void readFormatInputTest1x100() {
        String answer = "";
        for (int i = 0; i < 100; i++) {
            answer += ".";
        }
        String tester  = "1 100\n";
        tester += answer;
        tester += "\n0 0";
        ByteArrayInputStream in = new ByteArrayInputStream(tester.getBytes());
        System.setIn(in);
        Scanner sc = new Scanner(System.in);
        readFormatInput(sc);
        String testString = "";
        if (!INFIELDS.isEmpty()) {
            char[][] test = OfficialMinesweeper.INFIELDS.get(1);
            for (int j = 0; j < 100; j++) {
                testString += test[j][0];
            }
        }
        assertEquals(answer, testString);
    }


    @Test
    public void readFormatInputTest100x1() {
        String answer = "";
        for (int i = 0; i < 100; i++) {
            answer += ".\n";
        }
        String tester  = "100 1\n";
        tester += answer;
        tester += "0 0";
        InputStream sysInBackup = System.in; // backup System.in to restore it later
        ByteArrayInputStream in = new ByteArrayInputStream(tester.getBytes());
        System.setIn(in);
        Scanner sc = new Scanner(System.in);

        readFormatInput(sc);
        String testString = "";
        if (!INFIELDS.isEmpty()) {
            char[][] test = OfficialMinesweeper.INFIELDS.get(1);
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 1; j++) {
                    testString += test[i][j];
                }
                testString += "\n";
            }
        }
        testString.substring(0, testString.length() - 1);
        assertEquals(answer, testString);
    }
    @Test
    public void readFormatInputTest0x0() {
        String answer = "";

        String tester = "0 0";
        ByteArrayInputStream in = new ByteArrayInputStream(tester.getBytes());
        System.setIn(in);

        Scanner sc = new Scanner(System.in);

        readFormatInput(sc);
        String testString = "";
        if (!INFIELDS.isEmpty()) {
            char[][] test = OfficialMinesweeper.INFIELDS.get(2);
        }
        assertEquals(answer, testString);
    }

    @Test
    public void computeOutputTest2x2() {
        System.setOut(new PrintStream(outContent));
        char[][] test = computeOutput(INFIELDS.get(0));

        String answer = "00\n00";
        String testS = "";
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
               testS += test[i][j];
            }
            testS += "\n";
        }
        testS = testS.substring(0, testS.length() - 1);
        assertEquals(answer, testS);
    }
    @Test
    public void computeOutputTest100x1() {
        char[][] setup = new char[1][100];
        for (int i = 0; i < 1; i ++) {
            for (int j = 0; j < 100; j++) {
                setup[i][j] = '.';
            }
        }
        System.setOut(new PrintStream(outContent));
        char[][] test = computeOutput(setup);
        String answer = "";
        for (int j = 0; j < 1; j++) {
            for (int i = 0; i < 100; i++) {
                answer += "0";
            }
            answer += "\n";
        }
        String testS = "";
        for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 100; j++) {
            testS += test[i][j];
            }
            testS += "\n";
        }
        assertEquals(answer, testS);
    }
    @Test
    public void computeOutputTest1x100() {
        char[][] setup = new char[100][1];
        for (int i = 0; i < 100; i ++) {
            for (int j = 0; j < 1; j++) {
                setup[i][j] = '.';
            }
        }
        System.setOut(new PrintStream(outContent));
        char[][] test = computeOutput(setup);
        String answer = "";
        for (int j = 0; j < 100; j++) {
            answer += '0';
        }
        String testS = "";
            for (int j = 0; j < 100; j++) {
                testS += test[j][0];
            }
        assertEquals(answer, testS);
    }
    @Test
    public void writeOutputTest() {
        OUTFIELDS.clear();
        char[][] setup = new char[2][2];
        for (int i = 0; i < 2; i ++) {
            for (int j = 0; j < 2; j++) {
                setup[i][j] = '0';
            }
        }
        String answer = "Field #1:\n00\n00\n\r\n";
        System.setOut(new PrintStream(outContent));
        OUTFIELDS.add(setup);
        writeOutput();
        String test = String.valueOf(outContent);
        assertEquals(answer, test);
    }
}
