package atkinsonl;

import java.io.*;
import java.util.*;
//Logan Atkinson
//Instances must be input field by field sadly, but input output was correct.

//Output against official file shown in minesweeper_output_test.txt
//Visual proof of function in picture
public class logana {
        private int myCol;
        private int myRow;
        private static int fieldCounter = 1;
        private char myBoard[][];
        private ArrayList<Integer> mines;
        private String input;
        public static void main(String[] args) throws FileNotFoundException {
            //For Printing output to console
//        File file = new File("minesweeper_output_test.txt");
//        PrintStream printStreamToFile = new PrintStream(file);
//        System.setOut(printStreamToFile);
            Scanner sc;
            logana ms = new logana();
            while (true) {
                ms.input = "";
                if (args.length == 1){
                    sc = new Scanner(new File(args[0]));
                }
                else {
                    sc = new Scanner(System.in);
                }
                //ms.readInput();
                ms.fillBoard(sc);
                //ms.fillBoard(sc);
                ms.calculateBoard();
                ms.printBoard();
                fieldCounter++;
            }
        }
        //constructor for minesweeper
        public logana() {
            mines = new ArrayList<Integer>();
            input = "";
        }
//    public void readInput(){
//        Scanner s = new Scanner(System.in);
//        //System.out.println("ready");
//        while (s.hasNextLine()) {
//            String in = s.nextLine();
//            if (in == null || in.isEmpty())
//                return;
//            input += in;
//        }
//    }

        public void fillBoard(final Scanner sc2) throws FileNotFoundException {
            int counter = 0;
            myCol = Integer.parseInt(sc2.next());
            myRow = Integer.parseInt(sc2.next());
            if (myCol == 0 && myRow == 0) {
                System.exit(0);
            }
            myBoard = new char[myCol][myRow];
            sc2.nextLine();
            while (sc2.hasNextLine()) {
                String in = sc2.nextLine();

                for (int i = 0; i < myCol; i++) {
                    counter = 0;
                    for (int j = 0; j < myRow; j++) {
                        char c = in.charAt(counter);
                        counter++;
                        myBoard[i][j] = c;
                        if (c == '*') {
                            mines.add(i);
                            mines.add(j);
                        }
                        if (counter > myRow * myCol) {
                            return;
                        }
                    }
                    in = sc2.nextLine();
                }
                return;
            }
        }
        public void calculateBoard() {
            for (int i = 0; i < myCol; i++) {
                for (int j = 0; j < myRow; j++) {
                    if(myBoard[i][j] == '*') {
                        if (i > 0) {
                            if (myBoard[i - 1][j] == '.') {
                                myBoard[i - 1][j] = '1';
                            }
                            else if (myBoard[i - 1][j] != '*'){
                                int v = myBoard[i - 1][j] + 1;
                                myBoard[i - 1][j] = (char) v;
                            }
                        }
                        if (i < myCol - 1) {
                            if (myBoard[i + 1][j] == '.') {
                                myBoard[i + 1][j] = '1';
                            }
                            else if (myBoard[i + 1][j] != '*'){
                                int v = myBoard[i + 1][j] + 1;
                                myBoard[i + 1][j] = (char) v;
                            }
                        }
                        if (j > 0) {
                            if (myBoard[i][j - 1] == '.') {
                                myBoard[i][j - 1] = '1';
                            }
                            else if (myBoard[i][j - 1] != '*'){
                                int v = myBoard[i][j - 1] + 1;
                                myBoard[i][j - 1] = (char) v;
                            }
                        }
                        if (j < myRow - 1) {
                            if (myBoard[i][j + 1] == '.') {
                                myBoard[i][j + 1] = '1';
                            }
                            else if (myBoard[i][j + 1] != '*'){
                                int v = myBoard[i][j + 1] + 1;
                                myBoard[i][j + 1] = (char) v;
                            }
                        }
                        if (i < myCol - 1 && j > 0) {
                            if (myBoard[i + 1][j - 1] == '.') {
                                myBoard[i + 1][j - 1] = '1';
                            }
                            else if (myBoard[i + 1][j - 1] != '*'){
                                int v = myBoard[i + 1][j - 1] + 1;
                                myBoard[i + 1][j - 1] = (char) v;
                            }
                        }
                        if (j < myRow - 1 && i > 0) {
                            if (myBoard[i - 1][j + 1] == '.') {
                                myBoard[i - 1][j + 1] = '1';
                            }
                            else if (myBoard[i - 1][j + 1] != '*'){
                                int v = myBoard[i - 1][j + 1] + 1;
                                myBoard[i - 1][j + 1] = (char) v;
                            }
                        }
                        if (j < myRow - 1 && i < myCol - 1) {
                            if (myBoard[i + 1][j + 1] == '.') {
                                myBoard[i + 1][j + 1] = '1';
                            }
                            else if (myBoard[i + 1][j + 1] != '*'){
                                int v = myBoard[i + 1][j + 1] + 1;
                                myBoard[i + 1][j + 1] = (char) v;
                            }
                        }
                        if (j > 0 && i > 0) {
                            if (myBoard[i - 1][j - 1] == '.') {
                                myBoard[i - 1][j - 1] = '1';
                            }
                            else if (myBoard[i - 1][j - 1] != '*'){
                                int v = myBoard[i - 1][j - 1] + 1;
                                myBoard[i - 1][j - 1] = (char) v;
                            }
                        }
                    }
                    else if (myBoard[i][j] == '.') {
                        myBoard[i][j] = '0';
                    }
                }
            }
        }
        public void printBoard() {
            if (myCol == 0 && myRow == 0) {
                return;
            }
            System.out.println("Field # " + fieldCounter + ":");
            for (int i = 0; i < myCol; i++) {
                for (int j = 0; j < myRow; j++) {
                    System.out.print(myBoard[i][j]);
                }
                System.out.println();
            }
            System.out.println();
        }
        @Override
        public String toString() {
            return myBoard.toString();
        }
    }
